import matplotlib.pyplot as plt


# titre du graphique
plt.title("Représentation de l'absorbance\n en fonction de la concentration")

# Nom des axes et limites
plt.xlabel("Concentration $C$ en $\\mu$mol.L$^{-1}$")
plt.ylabel("Absorbance $A$")

plt.xlim(0,10)
plt.ylim(0,2)

# quadrillage
plt.grid(linestyle="--", color="blue")

# DOnnées expérimentales
C = [10,8,6,4,2]
A = [1.56, 1.24, 0.82, 0.62, 0.34]

# report des points sur le graphique
plt.plot(C,A,color="red",marker=".", linestyle="", label="expérience 1")
plt.legend()


# affichage
plt.show()
