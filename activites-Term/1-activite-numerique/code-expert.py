import matplotlib.pyplot as plt
import numpy as np


# titre du graphique
plt.title("Dosage conductimétrique")

# Nom des axes et limites
plt.xlabel("Concentration $C$ en mg.L$^{-1}$")
plt.ylabel("Conductivité en mS.cm$^{-1}$")

plt.xlim(200,1200)
plt.ylim(0.7,2.1)

# quadrillage
plt.grid(linestyle="--", color="gray")

# Données expérimentales
C = [1000,500,333,250]
Sigma = [2.04,1.21,0.90,0.76]

# report des points sur le graphique
plt.plot(C,Sigma,color="blue",marker="o", linestyle="", label="expérience 3 : points expérimentaux")

#################################################################
# modélisation par une droite : solution 1 avec la fonction poly1d
coeffs = np.polyfit(C,Sigma,1)
poly1 = np.poly1d(coeffs) # retourne l'équation de la droite ; en plus on peut calculer avec

C=np.array(C)
Sigma_modele = poly1(C)

# plt.plot(C,Sigma_modele,"r--",label=poly1)

##################################################################
# modélisation par une droite : solution 2 sans la fonction poly1d
coeffs = np.polyfit(C,Sigma,1)
a = coeffs[0]
b = coeffs[1]

C = np.array(C)
Sigma_modele = a*C + b

# plt.plot(C,Sigma_modele,"r--",label = "modèle : Sigma_modele = {a} x C + {b}".format(a = round(a,5), b = round(b,5)))



plt.legend()
# affichage
plt.show()

