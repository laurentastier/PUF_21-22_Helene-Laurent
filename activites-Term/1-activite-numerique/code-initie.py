import matplotlib.pyplot as plt


# titre du graphique
plt.title("Représentation de l'absorbance\n en fonction de la longueur d'onde")

# Nom des axes et limites
plt.xlabel("Longueur d'onde émise par la solution")
plt.ylabel("Absorbance $A$")

plt.ylim(0,0.6)
plt.xlim(300,800)

# quadrillage
plt.grid(linestyle="--", color="blue")

# DOnnées expérimentales
L = [390,420,450,480,510,540,570,600,630,660,690,720,750,780]
A = [0.12,0.15,0.20,0.39,0.51,0.39,0.08,0.03,0.03,0.03,0.02,0.02,0.02,0.02]

# report des points sur le graphique
plt.plot(L,A,color="red",marker=".", linestyle=":", label="expérience 2")
plt.legend()


# affichage
plt.show()

